require('dotenv').config();
const fetch = require('node-fetch');

const slackApi = function(){
    function leftpadTime(time) {
        if (time < 10) {
            return `0${time}`;
	}

        return time;
    }

    this.apiConfig = {
        host: process.env.API_HOST,
        token: process.env.SLACK_TOKEN,
    }

    this.requestConfig = {
        dnd: {
            path: {
                set: 'dnd.setSnooze',
                clear: 'dnd.endSnooze',
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Bearer ${this.apiConfig.token}`,
            },
        },
        profile: {
            path: {
                set: 'users.profile.set',
            },
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${this.apiConfig.token}`,
            },
            status: {
                emoji: process.env.STATUS_EMOJI,
            }
        },
    }

    this.setSnooze = async (amount) => {
        const params = {
            method: 'POST',
            headers: this.requestConfig.dnd.headers,
            body: `num_minutes=${amount}`,
        };

        return await fetch(`${this.apiConfig.host}${this.requestConfig.dnd.path.set}`, params)
        .then(res => res.json())
        .then(json => json);
    }

    /**
     * @todo: status message .env
     */
    this.setStatus = async (expirationTime) => {
        const params = {
            method: 'POST',
            headers: this.requestConfig.profile.headers,
            body: JSON.stringify({
                profile: {
                    status_text: `Doing pomodoro until ${expirationTime.hours()}:${leftpadTime(expirationTime.minutes())}`,
                    status_emoji: `:${this.requestConfig.profile.status.emoji}:`,
                    status_expiration: `${expirationTime.unix()}`
                }
            }),
        };

        return await fetch(`${this.apiConfig.host}${this.requestConfig.profile.path.set}`, params)
        .then(res => res.json())
        .then(json => json);
    }

    this.clearSnooze = async () => {
        const params = {
            method: 'POST',
            headers: this.requestConfig.dnd.headers,
        };

        return await fetch(`${this.apiConfig.host}${this.requestConfig.dnd.path.clear}`, params)
        .then(res => res.json())
        .then(json => json);
    }

    this.clearStatus = async () => {
        const params = {
            method: 'POST',
            headers: this.requestConfig.profile.headers,
            body: JSON.stringify({
                profile: {
                    status_text: '',
                    status_emoji: '',
                    status_expiration: '',
                    }
                }
            ),
        };

        return await fetch(`${this.apiConfig.host}${this.requestConfig.profile.path.set}`, params)
        .then(res => res.json())
        .then(json => json);
    }
}

module.exports = slackApi;
