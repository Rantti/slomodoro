var player = require('play-sound')(opts = {})

const readline = require('readline');
const moment = require('moment');
const fetch = require('node-fetch');

const token = process.env.SLACK_TOKEN;

const slackApi = require('./api/slack');
const api = new slackApi();

const prompt = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

/**
 * @todo: abstract to module
 */
async function getMinutes() {
  return new Promise((resolve, reject) => {
    prompt.question('How many minutes long break?', (amount) => {
      value = amount;
      prompt.close();
      resolve(amount);
    });
  });
}


async function init() {

  /**
   * @todo: abstract to timer module
   *
   * - init timer,
   */


  const [, , minutesArg] = process.argv;
  const minutesAmount = minutesArg || await getMinutes();
  const originalTime = moment();
  const endTime = moment().add(minutesAmount, 'minute');
  const originalDuration = moment.duration(endTime.diff(originalTime));

  api.setSnooze(minutesAmount);
  api.setStatus(endTime);

  /**
   * @todo: abstract to timer-module
   */
  const timer = setInterval(function () {
    const currentTime = moment();
    const timeTo = moment.duration(endTime.diff(currentTime));
    const remaining = originalDuration.asSeconds() - timeTo.asSeconds();

    const percentage = (remaining / originalDuration.asSeconds()) * 100;


    const timeString = `Current: ${currentTime.hours()}:${printTime(currentTime.minutes())}:${currentTime.seconds()} - Finish: ${endTime.hours()}:${printTime(endTime.minutes())}:${endTime.seconds()} = remaining: ${Math.round(percentage)} / 100%`;
    process.stdout.clearLine();
    process.stdout.cursorTo(0);

    function printTime(minutes) {
	if (minutes < 10) {
            return `0${minutes}`;
	}

	return minutes;
    }


    process.stdout.write(timeString);

    if (timeTo.asSeconds() <= 0) {
      clearInterval(timer);
      api.clearSnooze();
      api.clearStatus();
      console.log('finished!');
      player.play('./sounds/ring.mp3', function(err){
        if (err) throw err
      });
      process.exit();
    }
  }, 1000);
}

init();
