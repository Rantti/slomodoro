# Slomodoro ⏰

Keep track of your pomodoros and toggle your slack status based on that. Now as an npm-package!


## Requirements

### MPlayer

Slomodoro uses [play-sound](https://www.npmjs.com/package/play-sound) library to play music. They offer multiple player supports, but this has been primarily developed and tested with mplayer.

- For linux `$sudo apt install mplayer`.
- For windows, [download binary here](https://mplayerhq.hu/design7/dload.html#binaries)

### Slack

For slomodoro to work with your slack workspace, you need to generate yourself some API tokens.

If you do not have admin access to your slack workspace, you can still use [legacy slack tokens](https://api.slack.com/custom-integrations/legacy-tokens), but they will be deprecated in the future. Refer to documentation on [how to set up OAuth 2.0 for an application in your workspace](https://api.slack.com/docs/oauth)

## Setting up

1. Get the package with `$git clone git@github.com:Rantti/slomodoro.git`
2. Run `$npm install` to install required packages
3. In the root of the project folder copy your own .env template `$cp .env.template .env`, you can also use your shell enviroment variables if you want.
4. Add needed enviroment variables to the file
5. Run application with `$node index.js` in the project root. Enter your amount of minutes you want to do a pomodoro for, and check from your slack if it set your dnd on as supposed to.

## .env File

| Variable Name | Description                                             | Default Value                        |
|---------------|---------------------------------------------------------|--------------------------------------|
| STATUS_MSG    | Status message to show while in DND                     | "Pomodoring until [end of pomodoro]" |
| STATUS_EMOJI  | Name of emoji to use in status                          | tomato                               |
| SLACK_TOKEN   | Your Slack token, see "Slack" section above for details |                                      |

## TODO

 * Global executable ( e.g. `$slomodoro`, `$slomodoro 25`, `$slomodoro help`, `$slomodoro setup` )
 * 'timer animation', run down of current progress etc...
